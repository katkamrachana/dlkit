The Digital Learning Toolkit (dlkit) is a Python implementation of the OSIDs:

http://www.osid.org

DLKit has been written for Python 2.6 / Python 2.7.

General documentation can be found on readthedocs:

http://dlkit-doc.readthedocs.org/en/latest/

The current version can be found at:

https://github.mit.edu/sei/dlkit

Currently, DLKit relies on several submodules, though you can configure it differently,
using the runtime module.
