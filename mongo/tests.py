"""Mongo implementation tests"""
# pylint: skip-file
#    This is ugly and will all be replaced with generated tests

import pdb

import unittest
import random
from . import utilities
#from . import settings
from .primitives import *
from dlkit.abstract_osid.osid.errors import AlreadyExists, NotFound, OperationFailed, IllegalState
from .learning.managers import LearningManager
from .repository.managers import RepositoryManager
from .relationship.managers import RelationshipManager
from .assessment.managers import AssessmentManager, AssessmentProxyManager
from .proxy.managers import ProxyManager
from .type.managers import TypeManager
from .commenting.managers import CommentingManager
from ..abstract_osid.learning import sessions as abc_learning_sessions
from ..abstract_osid.learning import objects as abc_learning_objects
from ..abstract_osid.repository import objects as abc_repository_objects
from .proxy_example import TestRequest

DEFAULT_TYPE = Type(**{
    'authority': 'this.is.a.bogus.authority',
    'namespace': 'this.is.a.bogus.namespace',
    'identifier': 'mc3-objectivebank%3Amc3.learning.objectivebank.sandbox%40MIT-OEIT',
    'domain': '',
    'display_name': '',
    'description': '',
    'display_label': ''
    })

BANK_COLOR_RECORD_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'bank-record-type',
    'identifier': 'bank-color',
    'display_name': 'Bank Color',
    'display_label': 'Bank Color',
    'description': 'Assessment Bank record extension for Banks of color',
    'domain': 'assessment.Bank',
    })

VCB_ASSET_CONTENT_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'asset-content-record-type',
    'identifier': 'vcb-video-timestamp',
    'display_name': 'VCB video timestamps',
    'display_label': 'VCB video timestamps',
    'description': 'AssetContent record extension for storing start and end timestamps for video',
    'domain': 'repository.Asset'
    })

TEXT_ITEM_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'item-record-type',
    'identifier': 'item-text',
    'display_name': 'Item Text',
    'display_label': 'Item Text',
    'description': 'Assessment Item record extension for Items with text',
    'domain': 'assessment.Item',
    })

SHORT_TEXT_ANSWER_QUESTION_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'question-record-type',
    'identifier': 'short-text-answer',
    'display_name': 'Short Text Answer Question',
    'display_label': 'Short Text Answer Question',
    'description': 'Assessment Question record  for Questions that require a short text Answer',
    'domain': 'assessment.Question',
    })
    
SHORT_TEXT_ANSWER_TYPE = Type(**{
    'authority': 'ODL.MIT.EDU',
    'namespace': 'answer-record-type',
    'identifier': 'short-text-answer',
    'display_name': 'Short Text Answer',
    'display_label': 'Short Text Answer',
    'description': 'Assessment Answer record for Items that require a short text Answer',
    'domain': 'assessment.Answer',
    })

LABEL_ORTHO_FACES_QUESTION_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'question-record-type',
        'identifier': 'label-ortho-faces',
        'display_name': 'Label Orthographic Faces',
        'display_label': 'Label Orthographic Faces',
        'description': 'Assessment Question record extension for questions that require labeling of the faces of a 3D object',
        'domain': 'assessment.Question'
    })
    
LABEL_ORTHO_FACES_ANSWER_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'answer-record-type',
        'identifier': 'label-ortho-faces',
        'display_name': 'Label Orthographic Faces',
        'display_label': 'Label Orthographic Faces',
        'description': 'Assessment Answer record extension for answers that require labeling of the faces of a 3D object',
        'domain': 'assessment.Answer'
    })

EULER_ROTATION_QUESTION_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'question-record-type',
        'identifier': 'euler-rotation',
        'display_name': 'Euler Rotation',
        'display_label': 'Euler Rotation',
        'description': 'Assessment Question record extension for questions that require rotating a 3D object',
        'domain': 'assessment.Question',
    })
    
EULER_ROTATION_ANSWER_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'answer-record-type',
        'identifier': 'euler-rotation',
        'display_name': 'Euler Rotation',
        'display_label': 'Euler Rotation',
        'description': 'Assessment Answer record extension for answers that require rotating a 3D object',
        'domain': 'assessment.Answer',
    })

MULTI_CHOICE_ORTHO_QUESTION_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'question-record-type',
        'identifier': 'multi-choice-ortho',
        'display_name': 'Multiple Choice Ortho',
        'display_label': 'Multi Choice Ortho',
        'description': 'Assessment Question record extension for questions that require choosing from ortho projections',
        'domain': 'assessment.Question',
    })
    
MULTI_CHOICE_ORTHO_ANSWER_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'answer-record-type',
        'identifier': 'multi-choice-ortho',
        'display_name': 'Multiple Choice Ortho',
        'display_label': 'Multi Choice Ortho',
        'description': 'Assessment Answer record extension for answers that require choosing from ortho projections',
        'domain': 'assessment.Answer',
    })

FILE_SUBMISSION_QUESTION_RECORD_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'question-record-type',
        'identifier': 'file-submission',
        'display_name': 'File Submission Question',
        'display_label': 'File Submission Question',
        'description': 'Assessment Question record for file submission',
        'domain': 'assessment.Question',
    })

FILE_SUBMISSION_ANSWER_RECORD_TYPE = Type(**{
        'authority': 'ODL.MIT.EDU',
        'namespace': 'answer-record-type',
        'identifier': 'file-submission',
        'display_name': 'File Submission Answer',
        'display_label': 'File Submission Answer',
        'description': 'Assessment Answer record for file submission',
        'domain': 'assessment.Answer',
    })

REVIEW_OPTIONS_ASSESSMENT_TAKEN_TYPE = Type(**{
        'authority': 'MOODLE.ORG',
        'namespace': 'assessment-taken-record-type',
        'identifier': 'review-options',
        'display_name': 'Review Options',
        'display_label': 'Review Options',
        'description': 'AssessmentTaken record extension for that supports Moodle style review options',
        'domain': 'assessment.AssessmentTaken',
    })

REVIEW_OPTIONS_ASSESSMENT_OFFERED_TYPE = Type(**{
        'authority': 'MOODLE.ORG',
        'namespace': 'assessment-offered-record-type',
        'identifier': 'review-options',
        'display_name': 'Review Options',
        'display_label': 'Review Options',
        'description': 'AssessmentOffeed record extension for that supports Moodle style review options',
        'domain': 'assessment.AssessmentOffered',
   })


SAMPLE_LEARNING_OBJECTIVE_ID = Id(authority='Test', namespace="Test", identifier="Test")

class TestLearningManager(unittest.TestCase):

    def setUp(self):
        self.lm = LearningManager()

    def test_get_display_name(self):
        self.assertTrue(isinstance(
                self.lm.get_display_name().get_text(), str))

    def test_get_description(self):
        self.assertTrue(isinstance(
                self.lm.get_description().get_text(), str))

    def test_get_profile_language_type(self):
        self.assertTrue(isinstance(
                self.lm.get_display_name().get_language_type(), 
                Type))

    def test_get_profile_script_type(self):
        self.assertTrue(isinstance(
                self.lm.get_display_name().get_script_type(), 
                Type))

    def test_get_profile_format_type(self):
        self.assertTrue(isinstance(
                self.lm.get_display_name().get_format_type(), 
                Type))

    def test_display_name_turtles(self):
        self.assertTrue(isinstance(
                self.lm.get_display_name().get_language_type().get_display_name().get_script_type().get_display_label().get_text(),
                basestring)) # when handcar reports type domains again, include that too

    def test_supports_objective_bank_lookup(self):
        self.assertTrue(self.lm.supports_objective_bank_lookup())

    def test_supports_objective_lookup(self):
        self.assertTrue(self.lm.supports_objective_lookup())

    def test_supports_objective_query(self):
        self.assertFalse(self.lm.supports_objective_query())

    def test_get_objective_bank_lookup_session(self):
        obls = self.lm.get_objective_bank_lookup_session()
        self.assertTrue(isinstance(obls, 
                        abc_learning_sessions.ObjectiveBankLookupSession))

    def test_get_objective_lookup_session(self):
        ols = self.lm.get_objective_bank_lookup_session()
        self.assertTrue(isinstance(ols, 
                        abc_learning_sessions.ObjectiveBankLookupSession))

    def test_get_objective_lookup_session_for_objective_bank(self):
        ols = self.lm.get_objective_lookup_session()
        ols_for_ob = self.lm.get_objective_lookup_session_for_objective_bank(ols.get_objective_bank_id())
        self.assertTrue(ols.get_objective_bank_id().get_identifier() == 
                        ols_for_ob.get_objective_bank_id().get_identifier())


class TestObjectiveBankLookup(unittest.TestCase):

    def setUp(self):
        self.lm = LearningManager()
        self.obls = self.lm.get_objective_bank_lookup_session()

    def test_get_objective_banks(self):
        all_banks = self.obls.get_objective_banks()
        self.assertTrue(all_banks.available() > 0)

    def test_get_objective_bank(self):
        all_banks = self.obls.get_objective_banks()
        self.assertTrue(all_banks.available() > 0)
        for bank in all_banks:
            self.assertTrue(isinstance(bank, abc_learning_objects.ObjectiveBank))

    def test_get_objective_banks_by_ids(self):
        from .id import objects as id_objects
        all_banks = self.obls.get_objective_banks()
        all_banks_count = all_banks.available()
        all_bank_ids = []
        for bank in all_banks:
            all_bank_ids.append(bank.get_id())
        id_list = id_objects.IdList(all_bank_ids)
        self.assertTrue(id_list.available() == all_banks_count)
        all_banks_by_ids = self.obls.get_objective_banks_by_ids(id_list)
        self.assertTrue(all_banks_by_ids.available() == all_banks_count)
        for bank in all_banks_by_ids:
            self.assertTrue(isinstance(bank, abc_learning_objects.ObjectiveBank))

# if __name__ == '__main__':
#     unittest.main()

"""
class TestIdEquality(unittest.TestCase):
    def setUp(self):
        self.equiv_id1 = utilities.get_objective_bank_by_name('Second Test Objective Bank').ident
        self.equiv_id2 = utilities.get_objective_bank_by_name('Second Test Objective Bank').ident
        self.non_equiv_id = utilities.get_objective_bank_by_name('Third Objective Bank').ident

    def test_equivelant_ids(self):
        self.assertTrue(isinstance(self.equiv_id1, Id))
        self.assertTrue(isinstance(self.equiv_id2, Id))
        self.assertEqual(self.equiv_id1, self.equiv_id2)

    def test_non_equivelant_ids(self):
        self.assertTrue(isinstance(self.equiv_id1, Id))
        self.assertTrue(isinstance(self.non_equiv_id, Id))
        self.assertNotEqual(self.equiv_id1, self.non_equiv_id)

    def test_list_inclusion(self):
        id_list = [self.equiv_id1, self.non_equiv_id]
        self.assertTrue(self.equiv_id2 in id_list)"""


class TestObjectiveLookup(unittest.TestCase):

    def setUp(self):
        from .types import Genus as TypeFactory
        from .learning import objects
        self.lm = LearningManager()
        self.obls = self.lm.get_objective_bank_lookup_session()
        # Get the first Objective Bank (I know... sketchy)
        all_banks = self.obls.get_objective_banks()
        for bank in all_banks:
            if bank.display_name.text == 'Python Test Sandbox':
                self.test_bank = bank
        self.ols = self.lm.get_objective_lookup_session_for_objective_bank(self.test_bank.get_id())
        # Get list of supported Objective Types (Note: This does not use the osid contract)
        self.objective_genus_types = []
        tfactory = TypeFactory()
        for t in tfactory.type_set:
            for d in tfactory.type_set[t]:
                self.objective_genus_types.append(Type(**tfactory.get_type_data(d)))

    def test_get_objective_bank(self):
        test_bank_id = self.ols.get_objective_bank().get_id().get_identifier()
        self.assertTrue(test_bank_id == self.test_bank.get_id().get_identifier())

    def test_get_objectives(self):
        objectives = self.ols.get_objectives()
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_objective(self):
        # Get a random Objective
        all_objectives = self.ols.get_objectives()
        all_objectives.skip(random.randint(0, all_objectives.available() - 1))
        rand_objective = all_objectives.get_next_objective()
        objective = self.ols.get_objective(rand_objective.get_id())
        self.assertTrue(objective.get_id().get_identifier() == 
                        rand_objective.get_id().get_identifier())

    def test_get_objectives_by_type(self):
        # Get a random Objective
        rand_type = self.objective_genus_types[random.randint(0, len(self.objective_genus_types) - 1)]
        objectives = self.ols.get_objectives_by_genus_type(rand_type)
        for objective in objectives:
            self.assertTrue(objective.get_genus_type().get_identifier() == rand_type.get_identifier())

    def test_objective_is_of_genus_type(self):
        # Get a random Objective
        rand_type = self.objective_genus_types[random.randint(0, len(self.objective_genus_types) - 1)]
        objectives = self.ols.get_objectives_by_genus_type(rand_type)
        for objective in objectives:
            self.assertTrue(objective.is_of_genus_type(rand_type))
#
# if __name__ == '__main__':
#     unittest.main()

"""
class TestObjectiveRequisite(unittest.TestCase):
    def setUp(self):
        from .learning import objects
        from .learning import managers
        self.lm = LearningManager()
        self.obls = self.lm.get_objective_bank_lookup_session()
        # Get the first Objective Bank (I know... sketchy)
        all_banks = self.obls.get_objective_banks()
        self.first_bank = all_banks.get_next_objective_bank()
        self.ors = self.lm.get_objective_requisite_session_for_objective_bank(self.first_bank.get_id())
        test_objective = utilities.get_objective_by_bank_id_and_name(
                         utilities.get_objective_bank_by_name('Crosslinks').ident,
                         'Definite integral')
        self.test_objective_copy_id = Id(
                            authority = test_objective.ident.authority,
                            namespace = test_objective.ident.namespace,
                            identifier = test_objective.ident.identifier)
        self.test_objective_id = test_objective.ident      
        req_objective = utilities.get_objective_by_bank_id_and_name(
                         utilities.get_objective_bank_by_name('Crosslinks').ident,
                         'Summation')
        self.req_objective_copy_id = Id(
                            authority = req_objective.ident.authority,
                            namespace = req_objective.ident.namespace,
                            identifier = req_objective.ident.identifier)
        self.req_objective_id = req_objective.ident

    def test_get_requisite_objectives(self):
        objectives = self.ors.get_requisite_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_all_requisite_objectives(self):
        objectives = self.ors.get_all_requisite_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_get_dependent_objectives(self):
        objectives = self.ors.get_dependent_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_is_objective_required(self):
        self.assertTrue(self.ors.is_objective_required(self.test_objective_id,
                                                       self.req_objective_id))

    def test_is_objective_required_copy(self):
        self.assertTrue(self.ors.is_objective_required(self.test_objective_copy_id,
                                                       self.req_objective_copy_id))

    def test_objective_id_copy(self):
        self.assertTrue(self.test_objective_id == self.test_objective_copy_id)

    def test_req_objective_id_copy(self):
        self.assertTrue(self.req_objective_id == self.req_objective_copy_id)

    def test_get_equivalent_objectives(self):
        # Don't know how to test this yet.  Probably need to replace all of the
        # Requisite tests with more of a CRuD pattern
        self.assertTrue(True)
        
if __name__ == '__main__':
    unittest.main()


class TestObjectiveHierarchy(unittest.TestCase):
    def setUp(self):
        from .learning import objects
        from .learning import managers
        self.lm = LearningManager()
        self.test_bank = utilities.get_objective_bank_by_name('Chemistry Bridge')
        self.ohs = self.lm.get_objective_hierarchy_session_for_objective_bank(self.test_bank.ident)
        self.test_objective = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Acids and Bases')
        self.test_objective_id = self.test_objective.ident
        self.test_parent = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Buffers: A study of Chemical Equilibria')
        self.test_parent_id = self.test_parent.ident
        self.test_child = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Strength of an acid/base')
        self.test_child_id = self.test_child.ident
        self.test_final_child = utilities.get_objective_by_bank_id_and_name(
                         self.test_bank.ident,
                         'Auto-Ionization of Water outcome 3D1')
        self.test_final_child_id = self.test_final_child.ident


    def test_get_root_objective_ids(self):
        from .id import objects as id_objects
        objective_ids = self.ohs.get_root_objective_ids()
        self.assertTrue(isinstance(objective_ids, id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, Id))

    def test_get_root_objectives(self):
        objectives = self.ohs.get_root_objectives()
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_has_parent_objectives(self):
        self.assertTrue(self.ohs.has_parent_objectives(self.test_objective_id))
        self.assertFalse(self.ohs.has_parent_objectives(self.test_parent_id))

    def test_is_parent_of_objective(self):
        self.assertTrue(self.ohs.is_parent_of_objective(self.test_parent_id,
                                                       self.test_objective_id))
        self.assertFalse(self.ohs.is_parent_of_objective(self.test_child_id,
                                                       self.test_objective_id))

    def test_get_parent_objective_ids(self):
        from .id import objects as id_objects
        objective_ids = self.ohs.get_parent_objective_ids(self.test_objective_id)
        self.assertTrue(isinstance(objective_ids, id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, Id))

    def test_get_parent_objectives(self):
        objectives = self.ohs.get_parent_objectives(self.test_objective_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))

    def test_has_child_objectives(self):
        self.assertTrue(self.ohs.has_child_objectives(self.test_objective_id))
        self.assertFalse(self.ohs.has_child_objectives(self.test_final_child_id))

    def test_is_child_of_objective(self):
        self.assertTrue(self.ohs.is_child_of_objective(self.test_child_id,
                                                       self.test_objective_id))
        self.assertFalse(self.ohs.is_child_of_objective(self.test_parent_id,
                                                       self.test_objective_id))

    def test_get_child_objective_ids(self):
        from .id import objects as id_objects
        objective_ids = self.ohs.get_child_objective_ids(self.test_objective_id)
        self.assertTrue(isinstance(objective_ids, id_objects.IdList))
        for objective_id in objective_ids:
            self.assertTrue(isinstance(objective_id, Id))

    def test_get_child_objectives(self):
        objectives = self.ohs.get_child_objectives(self.test_child_id)
        self.assertTrue(isinstance(objectives, abc_learning_objects.ObjectiveList))
        for objective in objectives:
            self.assertTrue(isinstance(objective, abc_learning_objects.Objective))
"""

class TestActivityLookup(unittest.TestCase):

    def setUp(self):
        from .learning import objects
        from .learning import managers
        from .types import Genus as TypeFactory
        self.lm = LearningManager()
        self.obls = self.lm.get_objective_bank_lookup_session()
        # Get the first Objective Bank (I know... sketchy)
        all_banks = self.obls.get_objective_banks()
        for bank in all_banks:
            if bank.display_name.text == 'Python Test Sandbox':
                self.test_bank = bank
        self.als = self.lm.get_activity_lookup_session_for_objective_bank(self.test_bank.get_id())
        # Get list of supported Activity Types (Note: This does not use the osid contract yet)
        self.activity_genus_types = []
        tfactory = TypeFactory()
        for t in tfactory.type_set:
            for d in tfactory.type_set[t]:
                self.activity_genus_types.append(Type(**tfactory.get_type_data(d)))

    def test_get_objective_bank(self):
        test_bank_id = self.als.get_objective_bank().get_id().get_identifier()
        self.assertTrue(test_bank_id == self.test_bank.get_id().get_identifier())

    def test_get_activities(self):
        activities = self.als.get_activities()
        self.assertTrue(isinstance(activities, abc_learning_objects.ActivityList))
        for activity in activities:
            self.assertTrue(isinstance(activity, abc_learning_objects.Activity))

    def test_get_activity(self):
        # Get a random Objective Bank
        all_activities = self.als.get_activities()
        all_activities.skip(random.randint(0, all_activities.available() - 1))
        rand_activity = all_activities.get_next_activity()
        activity = self.als.get_activity(rand_activity.get_id())
        self.assertTrue(activity.get_id().get_identifier() == 
                        rand_activity.get_id().get_identifier())

    def test_get_activities_by_type(self):
        # Get a random activity genus type
        rand_type = self.activity_genus_types[random.randint(0, len(self.activity_genus_types) - 1)]
        activities = self.als.get_activities_by_genus_type(rand_type)
        for activity in activities:
            self.assertTrue(activity.get_genus_type().get_identifier() == rand_type.get_identifier())

    def test_activity_is_of_genus_type(self):
        # Get a random activity genus type
        rand_type = self.activity_genus_types[random.randint(0, len(self.activity_genus_types) - 1)]
        activities = self.als.get_activities_by_genus_type(rand_type)
        for activity in activities:
            self.assertTrue(activity.is_of_genus_type(rand_type))

# if __name__ == '__main__':
#     unittest.main()

class TestObjectiveMetadata(unittest.TestCase):
    def setUp(self):
        from .learning import objects
        from .learning import managers
        from . import utilities
        self.lm = LearningManager()
        self.obls = self.lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.ols = self.lm.get_objective_lookup_session_for_objective_bank(self.test_bank.get_id())
        # Get a random Objective - Assumes there are some:
        all_objectives = self.ols.get_objectives()
        all_objectives.skip(random.randint(0, all_objectives.available() - 1))
        self.rand_objective = all_objectives.get_next_objective()
        self.oas = self.lm.get_objective_admin_session_for_objective_bank(self.test_bank.get_id())
        self.objective_form = self.oas.get_objective_form_for_create([])

    def test_get_journal_comment_metadata(self):
        cm = self.objective_form.get_journal_comment_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_display_name_metadata(self):
        cm = self.objective_form.get_display_name_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_description_metadata(self):
        cm = self.objective_form.get_description_metadata()
        self.assertEqual(cm.get_syntax(), 'STRING')

    def test_get_genus_type_metadata(self):
        cm = self.objective_form.get_genus_type_metadata()
        self.assertEqual(cm.get_syntax(), 'TYPE')

    def test_get_assessment_metadata(self):
        cm = self.objective_form.get_assessment_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')

    def test_get_cognitive_process_metadata(self):
        cm = self.objective_form.get_cognitive_process_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')

    def test_get_knowledge_category_metadata(self):
        cm = self.objective_form.get_knowledge_category_metadata()
        self.assertEqual(cm.get_syntax(), 'ID')

# if __name__ == '__main__':
#     unittest.main()

"""
class TestTypeCrUD(unittest.TestCase):
    def setUp(self):
        from .primitives import Type
        tm = TypeManager()
        self.tas = tm.get_type_admin_session()
        self.tls = tm.get_type_lookup_session()
        self.test_type = Type(**{
            'authority': 'test-authority',
            'namespace': 'test-namespace',
            'identifier': 'test-identifier',
            'display_name': 'CRuD-Test Type',
            'display_label': 'CRuD',
            'description': 'This is a Test Type for CRuD',
            'domain': 'Test Domain'
        })

    def test_crud_types(self):
        # test create:
        if not self.tls.has_type(self.test_type):
            tfc = self.tas.get_type_form_for_create(self.test_type)
            tfc.display_name = 'CRuD-Test Type'
            tfc.display_label = 'CRuD-Test'
            tfc.description = 'This is a Test Type for CRuD'
            tfc.domain = 'Test Domain'
            new_type = self.tas.create_type(tfc)
        else:
            new_type = self.tls.get_type(self.test_type)
        print 'NEW TYPE NAME', new_type.display_name.text
        self.assertEqual(new_type.display_name.text, 'CRuD-Test Type')
        self.assertEqual(new_type.display_label.text, 'CRuD-Test')
        self.assertEqual(new_type.description.text, 'This is a Test Type for CRuD')
        self.assertEqual(new_type.domain.text, 'Test Domain')
       # test update:
        tfu = self.tas.get_type_form_for_update(new_type)
        tfu.set_display_name('New Name for CRuD-Test Type')
        print tfu._my_map
        self.tas.update_type(tfu)
        updated_type_id = {'authority': new_type.get_authority(),
                           'namespace': new_type.get_identifier_namespace(),
                           'identifier': new_type.get_identifier()}
        updated_type = self.tls.get_type(**updated_type_id)
        ## THIS FAILS, But why?
        #self.assertEqual(updated_type.display_name.text, 'New Name for CRuD-Test Type')
        # test delete:
        self.tas.delete_type(updated_type)
        with self.assertRaises(NotFound):
            self.tls.get_type({'authority': updated_type.get_authority(),
                               'namespace': updated_type.get_identifier_namespace(),
                               'identifier': updated_type.get_identifier()})

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        try:
            self.tas.delete_type(self.test_type)
        except:
            print 'Could not delete test type'


# if __name__ == '__main__':
#     unittest.main()
"""

class TestObjectiveBankCrUD(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        self.obas = lm.get_objective_bank_admin_session()
        self.obls = lm.get_objective_bank_lookup_session()
        self.default_bank_type = DEFAULT_TYPE

    def test_crud_objective_banks(self):
        # test create:
        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'CRuD-Test Objective Bank'
        obfc.description = 'This is a Test Objective Bank for CRuD'
        new_objective_bank = self.obas.create_objective_bank(obfc)
        self.assertEqual(new_objective_bank.display_name.text, 'CRuD-Test Objective Bank')
        #print new_objective_bank._my_map
        # test update:
        new_objective_bank_id = new_objective_bank.get_id()
        obfu = self.obas.get_objective_bank_form_for_update(new_objective_bank_id)
        obfu.set_display_name('Even Newer Name for CRuD-Test Objective Bank')
        self.obas.update_objective_bank(obfu)
        updated_objective_bank_id = new_objective_bank_id
        self.assertEqual(self.obls.get_objective_bank(updated_objective_bank_id).display_name.text, 'Even Newer Name for CRuD-Test Objective Bank')
        # test delete:
        self.obas.delete_objective_bank(updated_objective_bank_id)
        with self.assertRaises(NotFound):
            self.obls.get_objective_bank(updated_objective_bank_id)

    def test_truth(self):
        self.assertTrue(True)

class TestAssessmentBankOfColorCrUD(unittest.TestCase):
    def setUp(self):
        am = AssessmentManager()
        self.bas = am.get_bank_admin_session()
        self.bls = am.get_bank_lookup_session()
        self.default_bank_type = DEFAULT_TYPE

    def test_crud_objective_banks_of_color(self):
        # test create:
        bfc = self.bas.get_bank_form_for_create([BANK_COLOR_RECORD_TYPE])
        bfc.display_name = 'CRuD-Test Bank of Color'
        bfc.description = 'This is a Test Bank of Color for CRuD'
        new_bank = self.bas.create_bank(bfc)
        self.assertEqual(new_bank.display_name.text, 'CRuD-Test Bank of Color')
        #print new_objective_bank._my_map
        # test update:
        new_bank_id = new_bank.get_id()
        bfu = self.bas.get_bank_form_for_update(new_bank_id)
        color_coordinate = RGBColorCoordinate('00FF99')
        bfu.set_color_coordinate(color_coordinate)
        self.bas.update_bank(bfu)
        updated_bank_id = new_bank_id
        self.assertEqual(self.bls.get_bank(updated_bank_id).color_coordinate.values, [0, 255, 153])
        #print self.bls.get_bank(updated_bank_id).object_map
        # test delete:
        self.bas.delete_bank(updated_bank_id)
        with self.assertRaises(NotFound):
            self.bls.get_bank(new_bank_id)

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()


class TestGetOrchestratedCatalog(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        rm = RepositoryManager()
        self.obas = lm.get_objective_bank_admin_session()
        self.obls = lm.get_objective_bank_lookup_session()
        self.default_bank_type = DEFAULT_TYPE
        self.rls = rm.get_repository_lookup_session()
        self.ras = rm.get_repository_admin_session()

        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'Orchestrate Test Objective Bank'
        obfc.description = 'This is a Test Objective Bank for Orchestration'
        self.objective_bank = self.obas.create_objective_bank(obfc)


    def test_get_orchestrated_repo(self):
        # get orchestraded repo:
        repo = self.rls.get_repository(self.objective_bank.ident)
        print repo.get_object_map()
        # delete repo:
        self.ras.delete_repository(repo.ident)
        with self.assertRaises(NotFound):
            self.rls.get_repository(repo.ident)

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        self.obas.delete_objective_bank(self.objective_bank.ident)
        with self.assertRaises(NotFound):
            self.obls.get_objective_bank(self.objective_bank.ident)

# if __name__ == '__main__':
#     unittest.main()


class TestObjectiveBankHierarchy(unittest.TestCase):

    def setUp(self):
        lm = LearningManager()
        self.obas = lm.get_objective_bank_admin_session()
        self.obls = lm.get_objective_bank_lookup_session()
        self.test_banks = {}
        for name in ['One', 'Two', 'Three']:
            obfc = self.obas.get_objective_bank_form_for_create([])
            obfc.display_name = name
            obfc.description = 'Test Objective Bank ' + name
            obfc.genus_type = DEFAULT_TYPE
            self.test_banks[name] = self.obas.create_objective_bank(obfc)
            
    def test_get_sessions(self):
        lm = LearningManager()
        obhs = lm.get_objective_bank_hierarchy_session()
        obhds = lm.get_objective_bank_hierarchy_design_session()
        self.assertTrue(obhs.can_access_objective_bank_hierarchy)
        self.assertTrue(obhds.can_modify_objective_bank_hierarchy)
        #print obhds.get_objective_bank_hierarchy().display_name.text
        #print obhs.get_objective_bank_hierarchy().description.text
        #print obhds.get_objective_bank_hierarchy().display_name.text
        #print obhds.get_objective_bank_hierarchy().description.text
        #print obhs.get_objective_bank_hierarchy_id()
        #print obhds.get_objective_bank_hierarchy_id()

    def test_set_root(self):
        lm = LearningManager()
        obhs = lm.get_objective_bank_hierarchy_session()
        obhds = lm.get_objective_bank_hierarchy_design_session()
        obhds.add_root_objective_bank(self.test_banks['One'].ident)
        # check what's happening:
        self.assertEqual(obhs.get_root_objective_bank_ids().available(), 1)
        # delete all roots
        for bid in obhs.get_root_objective_bank_ids():
            obhds.remove_root_objective_bank(bid)
        self.assertEqual(obhs.get_root_objective_bank_ids().available(), 0)

    def test_simple_hierarchy(self):
        lm = LearningManager()
        obhs = lm.get_objective_bank_hierarchy_session()
        obhds = lm.get_objective_bank_hierarchy_design_session()
        # Design hierarchy:
        obhds.add_root_objective_bank(self.test_banks['One'].ident)
        obhds.add_child_objective_bank(self.test_banks['One'].ident, self.test_banks['Two'].ident)
        obhds.add_child_objective_bank(self.test_banks['One'].ident, self.test_banks['Three'].ident)
        # Check what's happened:
        self.assertTrue(obhs.has_child_objective_banks(self.test_banks['One'].ident))
        self.assertEqual(obhs.get_child_objective_bank_ids(self.test_banks['One'].ident).available(), 2)
        child_banks = obhs.get_child_objective_banks(self.test_banks['One'].ident)
        print obhs.get_objective_bank_node_ids(self.test_banks['One'].ident, 0, 2, False).get_node_map()
        for bank in child_banks:
            self.assertTrue(bank.display_name.text in ['Two', 'Three'])
        self.assertTrue(obhs.has_parent_objective_banks(self.test_banks['Two'].ident))
        self.assertTrue(obhs.has_parent_objective_banks(self.test_banks['Three'].ident))
        # delete all hierarchy relationships:
        obhds.remove_child_objective_banks(self.test_banks['One'].ident)
        self.assertFalse(obhs.has_child_objective_banks(self.test_banks['One'].ident))
        obhds.remove_root_objective_bank(self.test_banks['One'].ident)

    def test_orphaned_children(self):
        lm = LearningManager()
        obhs = lm.get_objective_bank_hierarchy_session()
        obhds = lm.get_objective_bank_hierarchy_design_session()
        # Design hierarchy:
        obhds.add_root_objective_bank(self.test_banks['One'].ident)
        obhds.add_child_objective_bank(self.test_banks['One'].ident, self.test_banks['Two'].ident)
        obhds.add_child_objective_bank(self.test_banks['One'].ident, self.test_banks['Three'].ident)
        obhds.remove_root_objective_bank(self.test_banks['One'].ident) # Uh Oh!  What of the children!?!     
        # Check what's happened:
        self.assertEqual(obhs.get_root_objective_bank_ids().available(), 2)        
        # delete all hierarchy relationships:
        obhds.remove_root_objective_bank(self.test_banks['Two'].ident)
        obhds.remove_root_objective_bank(self.test_banks['Three'].ident)

    """
    def test_crud_objective_banks(self):
        # test create:
        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'CRuD-Test Objective Bank'
        obfc.description = 'This is a Test Objective Bank for CRuD'
        new_objective_bank = self.obas.create_objective_bank(obfc)
        self.assertEqual(new_objective_bank.display_name.text, 'CRuD-Test Objective Bank')
        #print new_objective_bank._my_map
        # test update:
        new_objective_bank_id = new_objective_bank.get_id()
        obfu = self.obas.get_objective_bank_form_for_update(new_objective_bank_id)
        obfu.set_display_name('Even Newer Name for CRuD-Test Objective Bank')
        self.obas.update_objective_bank(obfu)
        updated_objective_bank_id = new_objective_bank_id
        self.assertEqual(self.obls.get_objective_bank(updated_objective_bank_id).display_name.text, 'Even Newer Name for CRuD-Test Objective Bank')
        # test delete:
        self.obas.delete_objective_bank(updated_objective_bank_id)
        with self.assertRaises(NotFound):
            self.obls.get_objective_bank(updated_objective_bank_id)"""

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        lm = LearningManager()
        rm = RelationshipManager()
        obhs = lm.get_objective_bank_hierarchy_session()
        rls = rm.get_relationship_lookup_session_for_family(obhs.get_objective_bank_hierarchy_id())
        ras = rm.get_relationship_admin_session_for_family(obhs.get_objective_bank_hierarchy_id())
        for bank in self.test_banks:
            self.obas.delete_objective_bank(self.test_banks[bank].ident)
        relationships = rls.relationships
        #print "DELETING:", relationships.available(), "RELATIONSHIPS"
        for relationship in relationships:
            #print "DELETING:", relationship.display_name.text
            ras.delete_relationship(relationship.ident)

# if __name__ == '__main__':
#     unittest.main()


class TestObjectiveBankNotEmptyDelete(unittest.TestCase):
    def setUp(self):
        self.lm = LearningManager()
        self.obas = self.lm.get_objective_bank_admin_session()
        self.obls = self.lm.get_objective_bank_lookup_session()
        self.default_bank_type = DEFAULT_TYPE

    def test_delete_non_empty_objective_banks(self):
        # test create:
        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'CRuD-Test Non Empty Objective Bank Delete'
        obfc.description = 'This is a Test for deleting non-empty Objective Banks'
        objective_bank = self.obas.create_objective_bank(obfc)
        objective_bank_id = objective_bank.get_id()
        # This is a little test of ID str round trip:
        objective_bank_idstr = str(objective_bank_id)
        self.assertEqual(objective_bank_id, Id(objective_bank_idstr))
        # End round trip test
        oas = self.lm.get_objective_admin_session_for_objective_bank(objective_bank_id)
        ofc = oas.get_objective_form_for_create([])
        ofc.set_display_name('Test Objective')
        ofc.set_description('This is a Test Objective')
        objective = oas.create_objective(ofc)
        # test non-empty delete:
        with self.assertRaises(IllegalState):
            self.obas.delete_objective_bank(objective_bank_id)
        # test empty delete:
        oas.delete_objective(objective.get_id())
        self.obas.delete_objective_bank(objective_bank_id)
        with self.assertRaises(NotFound):
            self.obls.get_objective_bank(objective_bank_id)
        

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()


class TestObjectiveCrUD(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        obls = lm.get_objective_bank_lookup_session()
        try:
            test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        test_bank_id = test_bank.ident
        self.test_bank = obls.get_objective_bank(test_bank_id)
        self.assertEqual(self.test_bank.get_display_name().get_text(), 'Python Test Sandbox')
        self.ols = lm.get_objective_lookup_session_for_objective_bank(test_bank_id)
        self.oas = lm.get_objective_admin_session_for_objective_bank(test_bank_id)

    def test_crud_objective(self):
        # test create:
        ofc = self.oas.get_objective_form_for_create([])
        ofc.set_display_name('Test Objective')
        ofc.set_description('This is a Test Objective')
        new_objective = self.oas.create_objective(ofc)
        self.assertEqual(new_objective.get_display_name().get_text(), 'Test Objective')
        # test update:
        new_objective_id = new_objective.get_id()
#        self.ols.use_federated_objective_bank_view()
        ofu = self.oas.get_objective_form_for_update(new_objective_id)
        ofu.set_display_name('New Name for Test Objective')
        self.oas.update_objective(ofu)
        updated_objective_id = new_objective_id
        updated_objective = self.ols.get_objective(updated_objective_id)
        self.assertEqual(new_objective_id.get_identifier(), updated_objective_id.get_identifier())
        self.assertEqual(self.ols.get_objective(updated_objective_id).get_display_name().get_text(), 'New Name for Test Objective')
        # test delete:
        self.oas.delete_objective(updated_objective_id)
        with self.assertRaises(NotFound):
            self.ols.get_objective(updated_objective_id)

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()


class TestActivityCrUD(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        obls = lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        test_bank_id = self.test_bank.ident
        self.assertEqual(self.test_bank.get_display_name().get_text(), 'Python Test Sandbox')
        self.als = lm.get_activity_lookup_session_for_objective_bank(test_bank_id)
        self.aas = lm.get_activity_admin_session_for_objective_bank(test_bank_id)
        self.ols = lm.get_objective_lookup_session_for_objective_bank(test_bank_id)
        self.oas = lm.get_objective_admin_session_for_objective_bank(test_bank_id)
        ofc = self.oas.get_objective_form_for_create([])
        ofc.set_display_name('Activity CrUD Test Objective')
        ofc.set_description('This is a Test Objective for testing Activity CrUD')
        self.test_objective = self.oas.create_objective(ofc)
        self.test_objective_id = self.test_objective.get_id()
#        print '\nTEST BANK ID', str(test_bank_id)

    def test_crud_activities(self):
        # test create:
#        print 'TEST OBJECTIVE ID', str(self.test_objective_id), '\n'
        afc = self.aas.get_activity_form_for_create(self.test_objective_id, [])
#        print afc._my_map
        afc.set_display_name('Test Activity')
        afc.set_description('This is a Test Activity')
        new_activity = self.aas.create_activity(afc)
        self.assertEqual(new_activity.get_display_name().get_text(), 'Test Activity')
        # test update:
        new_activity_id = new_activity.get_id()
#        print 'NEW ACTIVITY ID', str(new_activity_id)
        afu = self.aas.get_activity_form_for_update(new_activity_id)
        afu.set_display_name('New Name for Test Activity')
        self.aas.update_activity(afu)
        updated_activity_id = new_activity_id
        updated_activity = self.als.get_activity(updated_activity_id)
        self.assertEqual(new_activity_id.get_identifier(), updated_activity_id.get_identifier())
        self.assertEqual(self.als.get_activity(updated_activity_id).get_display_name().get_text(), 'New Name for Test Activity')
        # test delete
#        self.aas.delete_activity(updated_activity_id)
#        with self.assertRaises(NotFound):
#            self.als.get_activity(updated_activity_id)

    def test_truth(self):
        self.assertTrue(True)

#    def tearDown(self):
#        self.oas.delete_objective(self.test_objective_id)
#        with self.assertRaises(NotFound):
#            self.ols.get_objective(self.test_objective_id)
"""
class TestObjectiveDesignAndSeq(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        obls = lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.test_bank_id = self.test_bank.ident
        self.ohds = lm.get_objective_hierarchy_design_session_for_objective_bank(self.test_bank_id)
        self.ohs = lm.get_objective_hierarchy_session_for_objective_bank(self.test_bank_id)
        self.oss = lm.get_objective_sequencing_session_for_objective_bank(self.test_bank_id)
        self.parent_objective = utilities.create_objective(self.test_bank_id, 
            'parent objective', 'parent objective for testing hierarchy design')
        self.child_objectives = list()
        i = 0 
        while i < 5:
            i += 1
            self.child_objectives.append(utilities.create_objective(self.test_bank_id, 
                'child objective ' + str(i), 
                '#' + str(i) + ' child objective for testing hierarchy design'))

    def test_add_remove_child_objective(self):
        for child_objective in self.child_objectives:
            try:
                self.ohds.add_child_objective(self.parent_objective.ident, 
                                              child_objective.ident)
            except AlreadyExists:
                pass
        self.assertEqual(len(self.ohs.get_child_objectives(self.parent_objective.ident)), 5)
        self.ohds.remove_child_objective(self.parent_objective.ident, self.child_objectives[0].ident)
        self.assertEqual(len(self.ohs.get_child_objectives(self.parent_objective.ident)), 4)
        self.oss.move_objective_ahead(self.parent_objective.ident,
                                      self.child_objectives[1].ident,
                                      self.child_objectives[3].ident)
        self.oss.move_objective_behind(self.parent_objective.ident,
                                       self.child_objectives[2].ident,
                                       self.child_objectives[3].ident)
        self.ohds.remove_child_objectives(self.parent_objective.ident)
        self.assertEqual(len(self.ohs.get_child_objectives(self.parent_objective.ident)), 0)

    def test_five_children_in_list(self):
        self.assertEqual(len(self.child_objectives), 5)

if __name__ == '__main__':
    unittest.main()

class TestObjectiveRequisiteAssignment(unittest.TestCase):
    def setUp(self):
        lm = LearningManager()
        obls = lm.get_objective_bank_lookup_session()
        try:
            self.test_bank = utilities.get_objective_bank_by_name('Python Test Sandbox')
        except NotFound:
            self.test_bank = utilities.create_sandbox_objective_bank('Python Test Sandbox', 'a sandbox objective bank for testing Python implementations.')
        self.test_bank_id = self.test_bank.ident
        self.oras = lm.get_objective_requisite_assignment_session_for_objective_bank(self.test_bank_id)
        self.ors = lm.get_objective_requisite_session_for_objective_bank(self.test_bank_id)
        self.dependent_objective = utilities.create_objective(self.test_bank_id, 
            'dependent objective', 'dependent objective for testing requisite assignment')
        self.required_objectives = list()
        i = 0 
        while i < 3:
            i += 1
            self.required_objectives.append(utilities.create_objective(self.test_bank_id, 
                'required objective ' + str(i), 
                '#' + str(i) + ' required objective for testing requisite assignment'))

    def test_requisite_assignment(self):
        for required_objective in self.required_objectives:
            try:
                self.oras.assign_objective_requisite(self.dependent_objective.ident, 
                                                     required_objective.ident)
            except AlreadyExists:
                pass
        self.assertEqual(len(self.ors.get_requisite_objectives(self.dependent_objective.ident)), 3)
        self.oras.unassign_objective_requisite(self.dependent_objective.ident, self.required_objectives[0].ident)
        self.assertEqual(len(self.ors.get_requisite_objectives(self.dependent_objective.ident)), 2)
        for required_objective in self.ors.get_requisite_objectives(self.dependent_objective.ident):
            self.oras.unassign_objective_requisite(self.dependent_objective.ident, required_objective.ident)
        self.assertEqual(len(self.ors.get_requisite_objectives(self.dependent_objective.ident)), 0)

    def test_five_children_in_list(self):
        self.assertEqual(len(self.required_objectives), 3)

if __name__ == '__main__':
    unittest.main()
"""

"""
class TestTypeLookup(unittest.TestCase):
    def setUp(self):
        tm = TypeManager()
        self.tas = tm.get_type_admin_session()
        self.tls = tm.get_type_lookup_session()
        self.test_type = Type(**{
            'authority': 'test-authority',
            'namespace': 'test-namespace',
            'identifier': 'test-identifier',
            'display_name': 'CRuD-Test Type',
            'display_label': 'CRuD',
            'description': 'This is a Test Type for CRuD',
            'domain': 'Test Domain'
        })
        if not self.tls.has_type(self.test_type):
            tfc = self.tas.get_type_form_for_create(self.test_type)
            tfc.display_name = 'CRuD-Test Type'
            tfc.display_label = 'CRuD-Test'
            tfc.description = 'This is a Test Type for CRuD'
            tfc.domain = 'Test Domain'
            self.tas.create_type(tfc)

    def test_get_types(self):
        from ..abstract_osid.type import primitives as abc_type_primitives
        for t in self.tls.get_types():
            self.assertTrue(isinstance(t, abc_type_primitives.Type))

    def test_get_type(self):
        from ..abstract_osid.type import primitives as abc_type_primitives
        type_to_get = self.tls.get_types().get_next_type()
        type_elements = {'identifier': type_to_get.identifier,
                         'namespace': type_to_get.namespace,
                         'authority': type_to_get.authority,}
        self.assertTrue(isinstance(self.tls.get_type(**type_elements),
                                   abc_type_primitives.Type))

    def tearDown(self):
        try:
            self.tas.delete_type(self.test_type)
        except:
            print 'Could not delete test type'

# if __name__ == '__main__':
#     unittest.main()
"""

class TestRepositoryLookup(unittest.TestCase):

    def setUp(self):
        self.rm = RepositoryManager()
        self.rls = self.rm.get_repository_lookup_session()

    def test_repositories(self):
        all_repositories = self.rls.get_repositories()
        self.assertTrue(all_repositories.available() > 0)

    def test_get_repository(self):
        all_repositories = self.rls.get_repositories()
        self.assertTrue(all_repositories.available() > 0)
        for repository in all_repositories:
            self.assertTrue(isinstance(repository, abc_repository_objects.Repository))

    def test_get_repositories_by_ids(self):
        from .id import objects as id_objects
        all_repositories = self.rls.get_repositories()
        all_repositories_count = all_repositories.available()
        all_repository_ids = []
        for repository in all_repositories:
            all_repository_ids.append(repository.get_id())
        id_list = id_objects.IdList(all_repository_ids)
        self.assertTrue(id_list.available() == all_repositories_count)
        all_repositories_by_ids = self.rls.get_repositories_by_ids(id_list)
        self.assertTrue(all_repositories_by_ids.available() == all_repositories_count)
        for repository in all_repositories_by_ids:
            self.assertTrue(isinstance(repository, abc_repository_objects.Repository))

# if __name__ == '__main__':
#     unittest.main()

class TestRepositoryCrUD(unittest.TestCase):
    def setUp(self):
        rm = RepositoryManager()
        self.ras = rm.get_repository_admin_session()
        self.rls = rm.get_repository_lookup_session()
        self.default_repository_type = DEFAULT_TYPE

    def test_crud_repositories(self):
        # test create:
        rfc = self.ras.get_repository_form_for_create([])
        rfc.display_name = 'CrUD-Test Repository'
        rfc.description = 'This is a Test Repository for CrUD'
        new_repository = self.ras.create_repository(rfc)
        self.assertEqual(new_repository.display_name.text, 'CrUD-Test Repository')
        self.assertEqual(new_repository.description.text, 'This is a Test Repository for CrUD')
        # test update:
        new_repository_id = new_repository.get_id()
        rfu = self.ras.get_repository_form_for_update(new_repository_id)
        rfu.set_display_name('New Name for CrUD-Test Repository')
        self.ras.update_repository(rfu)
        updated_repository_id = new_repository_id
        self.assertEqual(self.rls.get_repository(updated_repository_id).display_name.text, 'New Name for CrUD-Test Repository')
        # test delete:
        self.ras.delete_repository(updated_repository_id)
        with self.assertRaises(NotFound):
            self.rls.get_repository(updated_repository_id)

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()

class TestFamilyCrUD(unittest.TestCase):
    def setUp(self):
        rm = RelationshipManager()
        self.fas = rm.get_family_admin_session()
        self.fls = rm.get_family_lookup_session()
        self.default_family_type = DEFAULT_TYPE

    def test_crud_families(self):
        # test create:
        ffc = self.fas.get_family_form_for_create([])
        ffc.display_name = 'CrUD-Test Family'
        ffc.description = 'This is a Test Family for CrUD'
        new_family = self.fas.create_family(ffc)
        self.assertEqual(new_family.display_name.text, 'CrUD-Test Family')
        self.assertEqual(new_family.description.text, 'This is a Test Family for CrUD')
        # test update:
        new_family_id = new_family.get_id()
        ffu = self.fas.get_family_form_for_update(new_family_id)
        ffu.set_display_name('New Name for CrUD-Test Family')
        self.fas.update_family(ffu)
        updated_family_id = new_family_id
        self.assertEqual(self.fls.get_family(updated_family_id).display_name.text, 'New Name for CrUD-Test Family')
        # test delete:
        self.fas.delete_family(updated_family_id)
        with self.assertRaises(NotFound):
            self.fls.get_family(updated_family_id)

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()


class TestAssetCrUD(unittest.TestCase):
    def setUp(self):
        rm = RepositoryManager()
        rls = rm.get_repository_lookup_session()
        try:
            test_repository = utilities.get_repository_by_name('Python Test Sandbox')
        except NotFound:
            test_repository = utilities.create_sandbox_repository('Python Test Sandbox', 'a sandbox catalog for testing Python implementations.')
        test_repository_id = test_repository.ident
        self.test_repository = rls.get_repository(test_repository_id)
        self.assertEqual(self.test_repository.get_display_name().get_text(), 'Python Test Sandbox')
        self.als = rm.get_asset_lookup_session_for_repository(test_repository_id)
        self.aas = rm.get_asset_admin_session_for_repository(test_repository_id)

    def test_crud_asset(self):
        # test create:
        afc = self.aas.get_asset_form_for_create([])
        afc.set_display_name('Test Asset')
        afc.set_description('This is a Test Asset')
        new_asset = self.aas.create_asset(afc)
        #print new_asset._my_map
        self.assertEqual(new_asset.get_display_name().get_text(), 'Test Asset')
        # test update:
        new_asset_id = new_asset.get_id()
        afu = self.aas.get_asset_form_for_update(new_asset_id)
        afu.set_display_name('New Name for Test Asset')
        self.aas.update_asset(afu)
        updated_asset_id = new_asset_id
        self.assertEqual(self.als.get_asset(updated_asset_id).get_display_name().get_text(), 'New Name for Test Asset')
        # test delete:
        self.aas.delete_asset(new_asset_id)
        with self.assertRaises(NotFound):
            self.als.get_asset(new_asset_id)

    def test_truth(self):
        self.assertTrue(True)

# if __name__ == '__main__':
#     unittest.main()


class TestAssetContentCrUD(unittest.TestCase):
    def setUp(self):
        rm = RepositoryManager()
        rls = rm.get_repository_lookup_session()
        try:
            test_repository = utilities.get_repository_by_name('Python Test Sandbox')
        except NotFound:
            test_repository = utilities.create_sandbox_repository('Python Test Sandbox', 'a sandbox catalog for testing Python implementations.')
        test_repository_id = test_repository.ident
        self.test_asset = utilities.create_asset(test_repository_id, 'AssetContent CrUD Asset', 'Asset for testing AssetContent CrUD')
        self.test_repository = rls.get_repository(test_repository_id)
        self.als = rm.get_asset_lookup_session_for_repository(test_repository_id)
        self.aas = rm.get_asset_admin_session_for_repository(test_repository_id)

    def test_crud_asset_content(self):
        # test create:
        from .primitives import DataInputStream
        acfc = self.aas.get_asset_content_form_for_create(self.test_asset.ident, [])
        acfc.set_display_name('Test Asset Content')
        acfc.set_description('This is a Test Asset Content')
        acfc.set_url('http://web.mit.edu')
        test_data = open('dlkit/mongo/test_file.py')
        acfc.set_data(DataInputStream(test_data))
        created_asset_content = self.aas.create_asset_content(acfc)
        new_asset = self.als.get_asset(self.test_asset.ident)
        new_asset_content = new_asset.get_asset_contents().next()
        self.assertEqual(new_asset_content.get_display_name().get_text(), 'Test Asset Content')
        self.assertEqual(new_asset_content.get_description().get_text(), 'This is a Test Asset Content')
        self.assertEqual(new_asset_content.get_url(), 'http://web.mit.edu')
        file_text = new_asset_content.get_data().read()
        self.assertEqual(file_text, '# Hi There')
        # test update:
        new_asset_content_id = new_asset_content.get_id()
        acfu = self.aas.get_asset_content_form_for_update(new_asset_content_id)
        acfu.set_display_name('New Name for Test Asset Content')
        acfu.set_description('This is an updated description for Test Asset Content')
        acfu.set_url('http://www.stanford.edu')
        updated_asset_content = self.aas.update_asset_content(acfu)
        updated_asset = self.als.get_asset(self.test_asset.ident)
        updated_asset_contents = updated_asset.get_asset_contents()
        self.assertEqual(updated_asset_contents.available(), 1)
        updated_asset_content = updated_asset_contents.next()
        self.assertEqual(updated_asset_content.get_display_name().get_text(), 'New Name for Test Asset Content')
        self.assertEqual(updated_asset_content.get_description().get_text(), 'This is an updated description for Test Asset Content')
        self.assertEqual(updated_asset_content.get_url(), 'http://www.stanford.edu')
        # test delete:
        self.aas.delete_asset_content(new_asset_content_id)
        self.assertEqual(self.als.get_asset(self.test_asset.ident).get_asset_contents().available(), 0)

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        self.aas.delete_asset(self.test_asset.ident)


# if __name__ == '__main__':
#     unittest.main()


class TestAssetContentCrUDWithRecord(unittest.TestCase):
    def setUp(self):
        rm = RepositoryManager()
        rls = rm.get_repository_lookup_session()
        try:
            test_repository = utilities.get_repository_by_name('Python Test Sandbox')
        except NotFound:
            test_repository = utilities.create_sandbox_repository('Python Test Sandbox', 'a sandbox catalog for testing Python implementations.')
        test_repository_id = test_repository.ident
        self.test_asset = utilities.create_asset(test_repository_id, 'AssetContent CrUD Asset', 'Asset for testing AssetContent with Record CrUD')
        self.test_repository = rls.get_repository(test_repository_id)
        self.als = rm.get_asset_lookup_session_for_repository(test_repository_id)
        self.aas = rm.get_asset_admin_session_for_repository(test_repository_id)

    def test_crud_asset_content(self):
        # test create:
        acfc = self.aas.get_asset_content_form_for_create(self.test_asset.ident, [VCB_ASSET_CONTENT_TYPE])
        acfc.set_display_name('Test VCB Asset Content')
        acfc.set_description('This is a Test of VCB Asset Content')
        acfc.set_url('http://web.mit.edu')
        ##
        # Tests Extensible:
        self.assertTrue(acfc.has_record_type(VCB_ASSET_CONTENT_TYPE))
        self.assertTrue(VCB_ASSET_CONTENT_TYPE in acfc.get_record_types())
        ##
#        print acfc._records
        ##
        # Tests OsidRecord pedentic:
        vcb_form = acfc.get_asset_content_form_record(VCB_ASSET_CONTENT_TYPE)
        self.assertTrue(vcb_form.implements_record_type(VCB_ASSET_CONTENT_TYPE))
#        print dir(vcb_form)
        vcb_form.set_start_timestamp(60)
        vcb_form.set_end_timestamp(360)
        created_asset_content = self.aas.create_asset_content(acfc)
        new_asset = self.als.get_asset(self.test_asset.ident)
        new_asset_content = new_asset.get_asset_contents().next()
#        print new_asset_content._my_map
        self.assertEqual(new_asset_content.get_display_name().get_text(), 'Test VCB Asset Content')
        self.assertEqual(new_asset_content.get_description().get_text(), 'This is a Test of VCB Asset Content')
        self.assertEqual(new_asset_content.get_url(), 'http://web.mit.edu')
        self.assertEqual(new_asset_content.get_start_timestamp(), 60)
        self.assertEqual(new_asset_content.get_end_timestamp(), 360)
        # test update streamlined:
        new_asset_content_id = new_asset_content.get_id()
        acfu = self.aas.get_asset_content_form_for_update(new_asset_content_id)
        acfu.set_display_name('New Name for Test Asset Content')
        acfu.set_description('This is an updated description for Test Asset Content')
        acfu.set_url('http://www.stanford.edu')
#        print dir(acfu)
        acfu.set_start_timestamp(70)
        acfu.set_end_timestamp(370)
        updated_asset_content = self.aas.update_asset_content(acfu)
        updated_asset = self.als.get_asset(self.test_asset.ident)
        updated_asset_contents = updated_asset.get_asset_contents()
        self.assertEqual(updated_asset_contents.available(), 1)
        updated_asset_content = updated_asset_contents.next()
        self.assertEqual(updated_asset_content.get_display_name().get_text(), 'New Name for Test Asset Content')
        self.assertEqual(updated_asset_content.get_description().get_text(), 'This is an updated description for Test Asset Content')
        self.assertEqual(updated_asset_content.get_url(), 'http://www.stanford.edu')
        self.assertEqual(updated_asset_content.get_start_timestamp(), 70)
        self.assertEqual(updated_asset_content.get_end_timestamp(), 370)
        # test delete:
        self.aas.delete_asset_content(new_asset_content_id)
        self.assertEqual(self.als.get_asset(self.test_asset.ident).get_asset_contents().available(), 0)

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        self.aas.delete_asset(self.test_asset.ident)

class TestSimpleItemCrUDWithRecord(unittest.TestCase):
    def setUp(self):
        self.am = AssessmentManager()
        try:
            test_bank = utilities.get_bank_by_name('Assessment Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_bank('Assessment Test Sandbox', 'a sandbox catalog for testing assessment extensions.')
        self.test_bank_id = test_bank.ident
        self.ils = self.am.get_item_lookup_session_for_bank(self.test_bank_id)
        self.ias = self.am.get_item_admin_session_for_bank(self.test_bank_id)

    def test_get_supported_record_types(self):
        for t in self.am.get_item_record_types():
            pass
            #print t.display_name.text

    def supports_record_types(self):
        self.assertTrue(self.am.supports_item_record_type(TEXT_ITEM_TYPE))

    def test_crud_text_item(self):
        # test create:
        ifc = self.ias.get_item_form_for_create([TEXT_ITEM_TYPE]) # Don't really need this Item record type
        ifc.set_display_name('Test Short Text Answer Assessment Item')
        ifc.set_description('This is a Test of Short Text Answer Assessment Item')
        ifc.set_learning_objectives([SAMPLE_LEARNING_OBJECTIVE_ID])
        ##
        # Tests Extensible:
        self.assertTrue(ifc.has_record_type(TEXT_ITEM_TYPE))
        self.assertTrue(TEXT_ITEM_TYPE in ifc.get_record_types())
        self.assertFalse(ifc.is_for_update())
        ##
        #print ifc._records
        #print ifc._my_map
        ##
        # Tests OsidRecord pedentic:
#        print dir(rs_form)
        new_item = self.ias.create_item(ifc)
#        print new_asset_content._my_map
        self.assertEqual(new_item.get_display_name().get_text(), 'Test Short Text Answer Assessment Item')
        self.assertEqual(new_item.get_description().get_text(), 'This is a Test of Short Text Answer Assessment Item')
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [SHORT_TEXT_ANSWER_QUESTION_TYPE])
#        print qfc._my_map
        qfc.set_text('How many roads must a man walk down?')
        #print qfc._my_map
        new_question = self.ias.create_question(qfc)
        self.assertEqual(new_question.display_name.text, 'Test Short Text Answer Assessment Item')
        self.assertEqual(new_question.description.text, 'This is a Test of Short Text Answer Assessment Item')
        self.assertTrue(new_question.has_text())
        self.assertEqual(new_question.get_text().text, 'How many roads must a man walk down?')
        self.assertEqual(new_question.get_learning_objective_ids().next(), SAMPLE_LEARNING_OBJECTIVE_ID)
        qfu = self.ias.get_question_form_for_update(new_question.ident)
        qfu.set_text('What is the answer to the question of life, the Universe and everything?')
        self.ias.update_question(qfu)
        # test create question streamlined:
        afc = self.ias.get_answer_form_for_create(new_item.ident, [SHORT_TEXT_ANSWER_TYPE])
        #print afc._my_map
        afc.set_text('42')
        new_answer = self.ias.create_answer(afc)
        reloaded_item = self.ils.get_item(new_item.ident)
        #print reloaded_item.get_display_name().get_text()
        #print reloaded_item.get_object_map()

    def test_crud_file_submission_item(self):
        # test create:
        ifc = self.ias.get_item_form_for_create([])
        ifc.set_display_name('Test File Submission Assessment Item')
        ifc.set_description('This is a Test of File Submission Answer Assessment Item')
        ##
        #print ifc._records
        #print ifc._my_map
        ##
        # Tests OsidRecord pedentic:
        new_item = self.ias.create_item(ifc)
#        print new_asset_content._my_map
        self.assertEqual(new_item.get_display_name().get_text(), 'Test File Submission Assessment Item')
        self.assertEqual(new_item.get_description().get_text(), 'This is a Test of File Submission Answer Assessment Item')
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [FILE_SUBMISSION_QUESTION_RECORD_TYPE])
#        print qfc._my_map
        qfc.set_text('Please prepare a file and submit it')
        #print qfc._my_map
        new_question = self.ias.create_question(qfc)
        self.assertEqual(new_question.display_name.text, 'Test File Submission Assessment Item')
        self.assertEqual(new_question.description.text, 'This is a Test of File Submission Answer Assessment Item')
        self.assertTrue(new_question.has_text())
        self.assertEqual(new_question.get_text().text, 'Please prepare a file and submit it')
        qfu = self.ias.get_question_form_for_update(new_question.ident)
        qfu.set_text('Submit three page paper on why there are 42 roads to walk down')
        self.ias.update_question(qfu)
        # test create answer streamlined (even though this particular question type has no correct answer):
        afc = self.ias.get_answer_form_for_create(new_item.ident, [FILE_SUBMISSION_ANSWER_RECORD_TYPE])
        #print afc._my_map
        my_file = open('dlkit/mongo/test_file.py')
        afc.set_file((DataInputStream(my_file)))
        new_answer = self.ias.create_answer(afc)
        reloaded_item = self.ils.get_item(new_item.ident)
        #print reloaded_item.get_object_map()
        answer = reloaded_item.get_answers().get_next_answer()
        self.assertTrue(answer.has_file_asset())
        submission = answer.get_file()
        #print reloaded_item.get_display_name().get_text()
        #print reloaded_item.get_object_map()

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        try:
            test_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, 'Test Result String Assessment Item')
        except:
            print 'could not find test item to delete'
        else:
            try:
                self.ias.delete_item(test_item.ident)
            except:
                print 'could not delete test item'
        try:
            test_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, 'Test File Submission Assessment Item')
        except:
            print 'could not find test item to delete'
        else:
            try:
                self.ias.delete_item(test_item.ident)
            except:
                print 'could not delete test item'


if __name__ == '__main__':
    unittest.main()

class TestAssessmentBasicAuthoring(unittest.TestCase):
    def setUp(self):
        self.am = AssessmentManager()
        try:
            test_bank = utilities.get_bank_by_name('Assessment Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_bank('Assessment Test Sandbox', 'a sandbox catalog for testing assessment extensions.')
        self.test_bank_id = test_bank.ident
        self.ils = self.am.get_item_lookup_session_for_bank(self.test_bank_id)
        self.ias = self.am.get_item_admin_session_for_bank(self.test_bank_id)
        self.als = self.am.get_assessment_lookup_session_for_bank(self.test_bank_id)
        self.aas = self.am.get_assessment_admin_session_for_bank(self.test_bank_id)
        afc = self.aas.get_assessment_form_for_create([])
        afc.set_display_name('Test Assessment')
        afc.set_description('This is a Test of Assessment for Basic Authoring')
        self.test_items = []
        for number in ['One', 'Two', 'Three']:
            ifc = self.ias.get_item_form_for_create([])
            ifc.set_display_name('Test Assessment Item ' + number)
            ifc.set_description('This is a Test Item Called Number ' + number)
            self.test_items.append(ias.create_item(ifc))

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()


class TestAssessmentContentCrUDWithOrthoThreeDRecords(unittest.TestCase):
    def setUp(self):
        #pdb.set_trace()
        self.am = AssessmentManager()
        try:
            test_bank = utilities.get_bank_by_name('Assessment Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_bank('Assessment Test Sandbox', 'a sandbox catalog for testing assessment extensions.')
        self.test_bank_id = test_bank.ident
        self.ils = self.am.get_item_lookup_session_for_bank(self.test_bank_id)
        self.ias = self.am.get_item_admin_session_for_bank(self.test_bank_id)

    def test_get_supported_record_types(self):
        for t in self.am.get_item_record_types():
            pass
            #print t.display_name.text

    def supports_record_types(self):
        self.assertTrue(self.am.supports_question_record_type(LABEL_ORTHO_FACES_QUESTION_TYPE))
        self.assertTrue(self.am.supports_question_record_type(LABEL_ORTHO_FACES_ANSWER_TYPE))
        self.assertTrue(self.am.supports_question_record_type(EULER_ROTATION_QUESTION_TYPE))
        self.assertTrue(self.am.supports_question_record_type(EULER_ROTATION_ANSWER_TYPE))

    def test_crud_euler_rotation(self):
        print "TESTING test_crud_euler_rotation"
        # test create:
        ifc = self.ias.get_item_form_for_create([])
        ifc.set_display_name('Test Label Ortho Faces Assessment Item')
        ifc.set_description('This is a Test of Label Ortho Faces Assessment Item')
        ##
        # Tests Extensible:
        ##
#        print ifc._records
        ##
        # Tests OsidRecord pedentic:
#        rs_form = ifc.get_item_form_record(RESPONSE_STRING_ITEM_TYPE)
#        self.assertTrue(rs_form.implements_record_type(RESPONSE_STRING_ITEM_TYPE))
#        print dir(rs_form)
        new_item = self.ias.create_item(ifc)
        self.new_item_id = new_item.ident
#        print new_asset_content._my_map
        self.assertEqual(new_item.get_display_name().get_text(), 'Test Label Ortho Faces Assessment Item')
        self.assertEqual(new_item.get_description().get_text(), 'This is a Test of Label Ortho Faces Assessment Item')
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [EULER_ROTATION_QUESTION_TYPE])
        qfc.set_text('Rotate the object to match the orthographic projections.')
        self.assertTrue(qfc.has_record_type(EULER_ROTATION_QUESTION_TYPE))
        self.assertTrue(EULER_ROTATION_QUESTION_TYPE in qfc.get_record_types())
        my_manip = open('dlkit/mongo/test_file.py')
        qfc.set_manip((DataInputStream(my_manip)))
        #qfc.add_asset(DataInputStream(my_manip), label="TestAsset")
        qfc.set_first_angle_projection(False)
#        print 'QFC MY MAP', qfc._my_map
        new_question = self.ias.create_question(qfc)
        #print new_question.get_object_map()
        self.assertFalse(new_question.is_first_angle_projection())
        self.assertEqual(new_question.display_name.text, 'Test Label Ortho Faces Assessment Item')
        self.assertEqual(new_question.description.text, 'This is a Test of Label Ortho Faces Assessment Item')
        qfu = self.ias.get_question_form_for_update(new_question.ident)
        newer_question = self.ias.update_question(qfu)
        #print newer_question._my_map
        self.assertEqual(newer_question.get_text().text, 'Rotate the object to match the orthographic projections.')
        # test create answer streamlined:
        afc = self.ias.get_answer_form_for_create(new_item.ident, [EULER_ROTATION_ANSWER_TYPE])
        afc.set_euler_angle_values(-45, 180, -90)
        new_answer = self.ias.create_answer(afc)
        reloaded_item = self.ils.get_item(new_item.ident)
        #print reloaded_item.get_object_map()
        reloaded_answer = reloaded_item.get_answers().next()
        self.assertEqual(reloaded_answer.get_x_angle_value(), -45)

    def test_crud_label_ortho(self):
        print "TESTING test_crud_label_ortho"
        # test create:
        ifc = self.ias.get_item_form_for_create([])
        ifc.set_display_name('Test Euler Rotation Assessment Item')
        ifc.set_description('This is a Test of Euler Rotation Assessment Item')
        ##
        # Tests Extensible:
        ##
#        print ifc._records
        ##
        # Tests OsidRecord pedentic:
#        rs_form = ifc.get_item_form_record(RESPONSE_STRING_ITEM_TYPE)
#        self.assertTrue(rs_form.implements_record_type(RESPONSE_STRING_ITEM_TYPE))
#        print dir(rs_form)
        new_item = self.ias.create_item(ifc)
        self.new_item_id = new_item.ident
#        print new_asset_content._my_map
        self.assertEqual(new_item.get_display_name().get_text(), 'Test Euler Rotation Assessment Item')
        self.assertEqual(new_item.get_description().get_text(), 'This is a Test of Euler Rotation Assessment Item')
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [LABEL_ORTHO_FACES_QUESTION_TYPE])
        qfc.set_text('Identify the front, top and side faces to match the orthographic projections.')
        self.assertTrue(qfc.has_record_type(LABEL_ORTHO_FACES_QUESTION_TYPE))
        self.assertTrue(LABEL_ORTHO_FACES_QUESTION_TYPE in qfc.get_record_types())
        my_manip = open('dlkit/mongo/test_file.py')
        qfc.set_manip((DataInputStream(my_manip)))
        qfc.set_first_angle_projection(True)
#        print 'QFC MY MAP', qfc._my_map
        new_question = self.ias.create_question(qfc)
        #print new_question.get_object_map()
        self.assertTrue(new_question.is_first_angle_projection())
        self.assertEqual(new_question.display_name.text, 'Test Euler Rotation Assessment Item')
        self.assertEqual(new_question.description.text, 'This is a Test of Euler Rotation Assessment Item')
        # test create answer streamlined:
        afc = self.ias.get_answer_form_for_create(new_item.ident, [LABEL_ORTHO_FACES_ANSWER_TYPE])
        afc.set_face_values(front_face_value = 1, side_face_value = 3, top_face_value = 5)
        new_answer = self.ias.create_answer(afc)
        reloaded_item = self.ils.get_item(new_item.ident)
        #print reloaded_item.get_object_map()
        reloaded_answer = reloaded_item.get_answers().next()
        self.assertEqual(reloaded_answer.get_front_face_value(), 1)


    def test_crud_multi_choice_otho(self):
        print "TESTING test_crud_multi_choice_otho"
        # test create:
        ifc = self.ias.get_item_form_for_create([])
        ifc.set_display_name('Test Multi Choice Ortho Assessment Item')
        ifc.set_description('This is a Test of Multi Choice Ortho Assessment Item')
        ##
        # Tests Extensible:
        ##
#        print ifc._records
        ##
        # Tests OsidRecord pedentic:
#        rs_form = ifc.get_item_form_record(RESPONSE_STRING_ITEM_TYPE)
#        self.assertTrue(rs_form.implements_record_type(RESPONSE_STRING_ITEM_TYPE))
#        print dir(rs_form)
        new_item = self.ias.create_item(ifc)
        self.new_item_id = new_item.ident
#        print new_asset_content._my_map
        self.assertEqual(new_item.get_display_name().get_text(), 'Test Multi Choice Ortho Assessment Item')
        self.assertEqual(new_item.get_description().get_text(), 'This is a Test of Multi Choice Ortho Assessment Item')
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [MULTI_CHOICE_ORTHO_QUESTION_TYPE])
        qfc.set_text('Choose the appropriate orthographic projection.')
        self.assertTrue(qfc.has_record_type(MULTI_CHOICE_ORTHO_QUESTION_TYPE))
        self.assertTrue(MULTI_CHOICE_ORTHO_QUESTION_TYPE in qfc.get_record_types())
        my_manip = open('dlkit/mongo/test_file.py')
        qfc.set_manip((DataInputStream(my_manip)))
        for name in ['One', 'Two', 'Three', 'Four']:
            qfc.set_ortho_choice(small_asset_data = DataInputStream(my_manip), large_asset_data = DataInputStream(my_manip), name=name)
        qfc.set_first_angle_projection(False)
#        print 'QFC MY MAP', qfc._my_map
        new_question = self.ias.create_question(qfc)
        new_question_map = new_question.get_object_map()
        print new_question_map
        self.assertFalse(new_question.is_first_angle_projection())
        self.assertEqual(new_question.display_name.text, 'Test Multi Choice Ortho Assessment Item')
        self.assertEqual(new_question.description.text, 'This is a Test of Multi Choice Ortho Assessment Item')
        qfu = self.ias.get_question_form_for_update(new_question.ident)
        newer_question = self.ias.update_question(qfu)
        self.assertEqual(newer_question.get_text().text, 'Choose the appropriate orthographic projection.')
        # test create answer streamlined:
        """
        afc = self.ias.get_answer_form_for_create(new_item.ident, [MULTI_CHOICE_ORTHO_ANSWER_TYPE])
        afc.set_euler_angle_values(-45, 180, -90)
        new_answer = self.ias.create_answer(afc)
        reloaded_item = self.ils.get_item(new_item.ident)
        #print reloaded_item.get_object_map()
        reloaded_answer = reloaded_item.get_answers().next()
        self.assertEqual(reloaded_answer.get_x_angle_value(), -45)"""



    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        ##
        # Delete the test assessment items
        #pdb.set_trace()
        stop = False
        while stop == False:
            for name in ['Test Label Ortho Faces Assessment Item', 'Test Euler Rotation Assessment Item']:
                try:
                    test_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, name)
                except:
                    stop = True
                else:
                    try:
                        print 'DELETING ' + name
                        self.ias.delete_item(test_item.ident)
                    except Exception as ex:
                        print ex
                        print 'could not delete test item: ' + name

if __name__ == '__main__':
    unittest.main()


class TestAssessmentOfferedCrUD(unittest.TestCase):
    def setUp(self):
        self.am = AssessmentManager()
        try:
            test_bank = utilities.get_bank_by_name('Assessment Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_bank('Assessment Test Sandbox', 'a sandbox catalog for testing assessment extensions.')
        self.test_bank_id = test_bank.ident
        self.als = self.am.get_assessment_lookup_session_for_bank(self.test_bank_id)
        self.aas = self.am.get_assessment_admin_session_for_bank(self.test_bank_id)
        self.aols = self.am.get_assessment_offered_lookup_session_for_bank(self.test_bank_id)
        self.aoas = self.am.get_assessment_offered_admin_session_for_bank(self.test_bank_id)
        afc = self.aas.get_assessment_form_for_create([])
        self.test_display_name = 'Test Assessment for testing Assessment Offered CRUD'
        self.test_description = 'This is a Test Assessment for testing Assessment Offered CRUD'
        afc.set_display_name(self.test_display_name)
        afc.set_description(self.test_description)
        self.test_assessment = self.aas.create_assessment(afc)

    def test_crud_assessment_offered(self):
        # test create:
        aofc = self.aoas.get_assessment_offered_form_for_create(self.test_assessment.ident, [REVIEW_OPTIONS_ASSESSMENT_OFFERED_TYPE])
        #aofc.display_name = 'Test AssessmentOffered'
        #aofc.description = 'This is a Test AssessmentOffered'
        aofc.set_start_time(DateTime(1, 1, 1, 1, 1))
        aofc.set_duration(Duration(7, 0, 0))
        aofc.set_review_whether_correct(during_attempt = False)
        aofc.set_max_attempts(2)
        new_assessment_offered = self.aoas.create_assessment_offered(aofc)
#        print new_assessment_offered._my_map
#        print new_assessment_offered.object_map
        #self.assertEqual(new_assessment_offered.get_display_name().get_text(), 'Test AssessmentOffered')
        self.assertEqual(new_assessment_offered.get_display_name().get_text(), self.test_display_name)
        self.assertEqual(new_assessment_offered.get_description().get_text(), self.test_description)
        self.assertEqual(new_assessment_offered.get_max_attempts(), 2)
        print new_assessment_offered.get_object_map()
        self.assertEqual(new_assessment_offered.get_start_time().month, 1)
        self.assertEqual(new_assessment_offered.get_duration().days, 7)
        self.assertFalse(new_assessment_offered.can_review_whether_correct_during_attempt())
        self.assertTrue(new_assessment_offered.can_review_whether_correct_after_attempt())
        found_assessment_offered = self.aols.get_assessments_offered_for_assessment(self.test_assessment.ident).next()
        self.assertEqual(found_assessment_offered.ident, new_assessment_offered.ident)
        self.aoas.delete_assessment_offered(new_assessment_offered.ident)
        """
        # test create question streamlined:
        qfc = self.ias.get_question_form_for_create(new_item.ident, [RESPONSE_STRING_QUESTION_TYPE])
        qfc.set_question_string('How many roads must a man walk down?')
        #print qfc._my_map
        new_question = self.ias.create_question(qfc)
        self.assertEqual(new_question.display_name.text, '')
        self.assertEqual(new_question.description.text, '')
        # test create question streamlined:
        afc = self.ias.get_answer_form_for_create(new_item.ident, [RESPONSE_STRING_ANSWER_TYPE])
        afc.set_response_string('42')
        new_answer = self.ias.create_answer(afc)
        reloaded_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, 'Test Result String Assessment Item')
        #print reloaded_item.get_display_name().get_text()
        #print reloaded_item.get_object_map()
        print reloaded_item.get_object_map()
        new_asset_content_id = new_asset_content.get_id()
        acfu = self.aas.get_asset_content_form_for_update(new_asset_content_id)
        acfu.set_display_name('New Name for Test Asset Content')
        acfu.set_description('This is an updated description for Test Asset Content')
        acfu.set_url('http://www.stanford.edu')
#        print dir(acfu)
        acfu.set_start_timestamp(70)
        acfu.set_end_timestamp(370)
        updated_asset_content = self.aas.update_asset_content(acfu)
        updated_asset = self.als.get_asset(self.test_asset.ident)
        updated_asset_contents = updated_asset.get_asset_contents()
        self.assertEqual(updated_asset_contents.available(), 1)
        updated_asset_content = updated_asset_contents.next()
        self.assertEqual(updated_asset_content.get_display_name().get_text(), 'New Name for Test Asset Content')
        self.assertEqual(updated_asset_content.get_description().get_text(), 'This is an updated description for Test Asset Content')
        self.assertEqual(updated_asset_content.get_url(), 'http://www.stanford.edu')
        self.assertEqual(updated_asset_content.get_start_timestamp(), 70)
        self.assertEqual(updated_asset_content.get_end_timestamp(), 370)
        # test delete:
        self.aas.delete_asset_content(new_asset_content_id)
        self.assertEqual(self.als.get_asset(self.test_asset.ident).get_asset_contents().available(), 0)
        """

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        try:
            test_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, 'Test Result String Assessment Item')
        except:
            print 'could not find test item to delete'
        else:
            try:
                self.ias.delete_item(test_item.ident)
            except:
                print 'could not delete test item'

if __name__ == '__main__':
    unittest.main()

class TestTakingAssessment(unittest.TestCase):
    def setUp(self):
        self.am = AssessmentManager()
        self.apm = AssessmentProxyManager()
        ps = ProxyManager().get_proxy_session()
        condition = ps.get_proxy_condition()
        request = TestRequest()
        condition.set_http_request(request)
        proxy = ps.get_proxy(condition)
        try:
            test_bank = utilities.get_bank_by_name('Assessment Test Sandbox')
        except NotFound:
            test_bank = utilities.create_sandbox_bank('Assessment Test Sandbox', 'a sandbox catalog for testing assessment extensions.')
        self.test_bank_id = test_bank.ident
        self.ils = self.am.get_item_lookup_session_for_bank(self.test_bank_id)
        self.ias = self.am.get_item_admin_session_for_bank(self.test_bank_id)
        self.als = self.am.get_assessment_lookup_session_for_bank(self.test_bank_id)
        self.aas = self.am.get_assessment_admin_session_for_bank(self.test_bank_id)
        self.aols = self.am.get_assessment_offered_lookup_session_for_bank(self.test_bank_id)
        self.atas = self.apm.get_assessment_taken_admin_session_for_bank(self.test_bank_id, proxy)
        self.atls = self.apm.get_assessment_taken_lookup_session_for_bank(self.test_bank_id, proxy)
        self.aoas = self.am.get_assessment_offered_admin_session_for_bank(self.test_bank_id)
        self.abas = self.am.get_assessment_basic_authoring_session_for_bank(self.test_bank_id)
        afc = self.aas.get_assessment_form_for_create([])
        afc.set_display_name('Test Assessment for testing Assessment Taking')
        afc.set_description('This is a Test Test Assessment for testing Assessment Taking')
        self.test_assessment = self.aas.create_assessment(afc)

        self.test_items = []
        answer_key = {'One': 'I', 'Two': 'II', 'Three': 'III'}
        for number in ['One', 'Two', 'Three']:
            ifc = self.ias.get_item_form_for_create([])
            #ifc.set_display_name('Test Assessment Item ' + number)
            ifc.display_name = 'Test Assessment Item ' + number
            ifc.set_description('This is a Test Item Called Number ' + number)
            test_item = self.ias.create_item(ifc)
            self.test_items.append(test_item)
            quesfc = self.ias.get_question_form_for_create(test_item.ident, [SHORT_TEXT_ANSWER_QUESTION_TYPE])
            ##
            # For some reason the property is not working. Why does it work for ifc.display_name above?
            # Comment/Uncomment next two lines to check:
            #quesfc.text = "What is the Roman Numeral representation of the number " + number
            quesfc.set_text("What is the Roman Numeral representation of the number " + number)
            #print quesfc._my_map
            question = self.ias.create_question(quesfc)
            quesfu = self.ias.get_question_form_for_update(question.ident)
            quesfu.set_display_name('Question ' + number)
            self.ias.update_question(quesfu)
            ansfc = self.ias.get_answer_form_for_create(test_item.ident, [SHORT_TEXT_ANSWER_TYPE])
            ansfc.set_text = answer_key[number]
            self.ias.create_answer(ansfc)
        for item in self.test_items:
            self.abas.add_item(self.test_assessment.ident, item.ident)
            #print self.ils.get_item(item.ident).get_question().get_text().text

    def test_assessment_items(self):
        self.assertEqual(self.abas.get_items(self.test_assessment.ident).available(), 3)

    def test_assessment_offered_persistence(self):
        aof = self.aoas.get_assessment_offered_form_for_create(self.test_assessment.ident, [REVIEW_OPTIONS_ASSESSMENT_OFFERED_TYPE])
        # It turns out that when datetimes are stored in mongo, bson
        # can only deal with milliseconds, so microseconds get truncated:
        start_time = DateTime.now().replace(microsecond = 0)
        duration = Duration(3, 0, 0)
        aof.display_name = 'Test Assessment Offered for taking Assessments'
        aof.set_items_sequential(True)
        aof.set_start_time(start_time)
        aof.set_duration(duration)
        aof.set_max_attempts(0)
        # also include grade system and score system checks someday
        ao = self.aoas.create_assessment_offered(aof)
        test_ao = self.aols.get_assessment_offered(ao.ident)
        self.assertTrue(test_ao.are_items_sequential)
        self.assertTrue(test_ao.has_start_time)
        self.assertEqual(test_ao.start_time, start_time)
        self.assertTrue(test_ao.has_duration)
        self.assertEqual(test_ao.duration, duration)
        #print test_ao._my_map
    
    def test_assessment_taken(self):
        aof = self.aoas.get_assessment_offered_form_for_create(self.test_assessment.ident, [REVIEW_OPTIONS_ASSESSMENT_OFFERED_TYPE])
        # It turns out that when datetimes are stored in mongo, bson
        # can only deal with milliseconds, so microseconds get truncated:
        start_time = DateTime.now().replace(microsecond = 0)
        end_second = start_time.second + 2
        deadline = DateTime.now().replace(second = end_second, microsecond = 0)
        aof.display_name = 'Test Assessment Offered for taking Assessments'
        aof.set_items_sequential(True)
        aof.set_start_time(start_time)
        aof.set_deadline(deadline)
        aof.set_review_whether_correct(during_attempt = False, after_attempt = True)
        aof.set_max_attempts(1)
        ao = self.aoas.create_assessment_offered(aof)
        test_ao = self.aols.get_assessment_offered(ao.ident)
        atf = self.atas.get_assessment_taken_form_for_create(test_ao.ident, [REVIEW_OPTIONS_ASSESSMENT_TAKEN_TYPE])
        atf.display_name = 'Test Assessment Taken for taking Assessments'
        test_assessment_taken = self.atas.create_assessment_taken(atf)
        #print test_assessment_taken.get_object_map()['reviewWhetherCorrect']
        self.assertEqual(test_assessment_taken.get_taker_id().identifier, 'pwilkins@mit.edu')
        self.assertEqual(test_assessment_taken.get_taking_agent_id().identifier, 'pwilkins@mit.edu')
        self.assertTrue(test_assessment_taken.has_started())
        self.assertFalse(test_assessment_taken.has_ended())

    def test_assessment(self):
        import datetime
        aof = self.aoas.get_assessment_offered_form_for_create(self.test_assessment.ident, [REVIEW_OPTIONS_ASSESSMENT_OFFERED_TYPE])
        # It turns out that when datetimes are stored in mongo, bson
        # can only deal with milliseconds, so microseconds get truncated:
        start_time = DateTime.now().replace(microsecond = 0)
        et = start_time + datetime.timedelta(0, 2)
        deadline = DateTime(et.year, et.month, et.day, et.hour, et.minute, et.second, et.microsecond)
        aof.display_name = 'Test Assessment Offered for taking Assessments'
        aof.set_items_sequential(True)
        aof.set_start_time(start_time)
        aof.set_deadline(deadline)
        ao = self.aoas.create_assessment_offered(aof)
        test_ao = self.aols.get_assessment_offered(ao.ident)
        atf = self.atas.get_assessment_taken_form_for_create(test_ao.ident, [])
        atf.display_name = 'Test Assessment Taken for taking Assessments'
        test_assessment_taken = self.atas.create_assessment_taken(atf)
        test_assessment_taken_id = test_assessment_taken.ident
        ass = self.am.get_assessment_session_for_bank(self.test_bank_id)
        self.assertTrue(ass.has_assessment_begun(test_assessment_taken_id))
        self.assertFalse(ass.is_assessment_over(test_assessment_taken_id))
        first_section = ass.get_first_assessment_section(test_assessment_taken_id)
        first_section_id = first_section.ident
        self.assertFalse(ass.has_next_assessment_section(first_section_id))
        self.assertFalse(ass.has_previous_assessment_section(first_section_id))
        assessment_section = ass.get_assessment_section(first_section_id)
        self.assertEqual(assessment_section.ident, first_section_id)
        assessment_section = ass.get_assessment_sections(test_assessment_taken_id).next()
        assessment_section_id = assessment_section.get_id()
        self.assertEqual(assessment_section_id, first_section_id)
        self.assertFalse(ass.is_assessment_section_complete(assessment_section_id))
        self.assertEqual(ass.get_incomplete_assessment_sections(test_assessment_taken_id).available(), 1)
        self.assertEqual(ass.get_incomplete_assessment_sections(test_assessment_taken_id).next().ident, first_section_id)
        self.assertTrue(ass.has_assessment_section_begun(assessment_section_id))
        self.assertFalse(ass.is_assessment_section_over(assessment_section_id))
        # test requires_synchronous_responses
        first_question = ass.get_first_question(assessment_section_id)
        self.assertEquals(first_question.text.text,'What is the Roman Numeral representation of the number One')
        self.assertTrue(ass.has_next_question(assessment_section_id, first_question.ident))
        second_question = ass.get_next_question(assessment_section_id, first_question.ident)
        self.assertEquals(second_question.get_text().text,'What is the Roman Numeral representation of the number Two')

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        ##
        # Delete the test assessments taken
        stop = False
        while stop == False:
            try:
                test_assessment_taken = utilities.get_assessment_taken_by_bank_id_and_name(self.test_bank_id, 'Test Assessment Taken for taking Assessments')
            except:
                stop = True
            else:
                try:
                    self.atas.delete_assessment_taken(test_assessment_taken.ident)
                except:
                    print 'could not delete test assessment taken'

        ##
        # Delete the test assessments offered
        stop = False
        while stop == False:
            try:
                test_assessment_offered = utilities.get_assessment_offered_by_bank_id_and_name(self.test_bank_id, 'Test Assessment Offered for taking Assessments')
            except:
                stop = True
            else:
                try:
                    self.aoas.delete_assessment_offered(test_assessment_offered.ident)
                except:
                    print 'could not delete test assessment offered'
                    raise

        ##
        # Delete the test assessment
        stop = False
        while stop == False:
            try:
                test_assessment = utilities.get_assessment_by_bank_id_and_name(self.test_bank_id, 'Test Assessment for testing Assessment Taking')
            except:
                stop = True
            else:
                try:
                    self.aas.delete_assessment(test_assessment.ident)
                except:
                    print 'could not delete test assessment'

        ##
        # Delete the test assessment items
        stop = False
        while stop == False:
            for number in ['One', 'Two', 'Three']:
                try:
                    test_item = utilities.get_item_by_bank_id_and_name(self.test_bank_id, 'Test Assessment Item ' + number)
                except:
                    stop = True
                else:
                    try:
                        self.ias.delete_item(test_item.ident)
                    except:
                        print 'could not delete test item ' + number


class TestComentCrUD(unittest.TestCase):

    def setUp(self):
        lm = LearningManager()
        cm = CommentingManager()
        self.obas = lm.get_objective_bank_admin_session()
        self.obls = lm.get_objective_bank_lookup_session()
        obfc = self.obas.get_objective_bank_form_for_create([])
        obfc.display_name = 'Test Objective Bank'
        obfc.description = 'Test Objective Bank for testing Commenting Service '
        obfc.genus_type = DEFAULT_TYPE
        self.test_bank = self.obas.create_objective_bank(obfc)
        self.clus = cm.get_comment_lookup_session_for_book(self.test_bank.ident)
        self.cas = cm.get_comment_admin_session_for_book(self.test_bank.ident)

    def test_crud_comment(self):
        # test create comment:
        cfc = self.cas.get_comment_form_for_create(self.test_bank.ident, [])
        cfc.set_display_name('Test Comment')
        cfc.set_description('This is a test Comment')
        cfc.set_text('Please allow me to comment on this thing.')
        new_comment = self.cas.create_comment(cfc)
        #print new_comment._my_map
        bank_comments = self.clus.get_comments_for_reference(self.test_bank.ident)
        self.assertEqual(bank_comments.available(), 1)
        comment = bank_comments.get_next_comment()
        print comment.object_map
        self.assertTrue(comment.get_start_date() < DateTime.now())
        self.assertEqual(comment.get_text().get_text(), 'Please allow me to comment on this thing.')
        #self.assertEqual(new_asset.get_display_name().get_text(), 'Test Asset')
        # test update:
        #new_asset_id = new_asset.get_id()
        #afu = self.aas.get_asset_form_for_update(new_asset_id)
        #afu.set_display_name('New Name for Test Asset')
        #self.aas.update_asset(afu)
        #updated_asset_id = new_asset_id
        #self.assertEqual(self.als.get_asset(updated_asset_id).get_display_name().get_text(), 'New Name for Test Asset')
        # test delete:
        #self.aas.delete_asset(new_asset_id)
        #with self.assertRaises(NotFound):
        #    self.als.get_asset(new_asset_id)

    def test_truth(self):
        self.assertTrue(True)

    def tearDown(self):
        for comment in self.clus.get_comments():
            #print "DELETING:", relationship.display_name.text
            self.cas.delete_comment(comment.ident)
        self.obas.delete_objective_bank(self.test_bank.ident)


if __name__ == '__main__':
    unittest.main()
