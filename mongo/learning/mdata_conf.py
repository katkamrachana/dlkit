"""Mongo osid metadata configurations for learning service."""

from .. import types
from ..primitives import Type
DEFAULT_LANGUAGE_TYPE = Type(**types.Language().get_type_data("DEFAULT"))
DEFAULT_SCRIPT_TYPE = Type(**types.Script().get_type_data("DEFAULT"))
DEFAULT_FORMAT_TYPE = Type(**types.Format().get_type_data("DEFAULT"))
DEFAULT_GENUS_TYPE = Type(**types.Genus().get_type_data("DEFAULT"))



OBJECTIVE_COGNITIVE_PROCESS = {
    'element_label': 'cognitive process',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

OBJECTIVE_ASSESSMENT = {
    'element_label': 'assessment',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

OBJECTIVE_KNOWLEDGE_CATEGORY = {
    'element_label': 'knowledge category',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}




ACTIVITY_COURSES = {
    'element_label': 'courses',
    'instructions': 'accepts an osid.id.Id[] object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': True,
    'default_id_values': [],
    'syntax': 'ID',
    'id_set': [],
}

ACTIVITY_ASSESSMENTS = {
    'element_label': 'assessments',
    'instructions': 'accepts an osid.id.Id[] object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': True,
    'default_id_values': [],
    'syntax': 'ID',
    'id_set': [],
}

ACTIVITY_OBJECTIVE = {
    'element_label': 'objective',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

ACTIVITY_ASSETS = {
    'element_label': 'assets',
    'instructions': 'accepts an osid.id.Id[] object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': True,
    'default_id_values': [],
    'syntax': 'ID',
    'id_set': [],
}




PROFICIENCY_COMPLETION = {
    'element_label': 'completion',
    'instructions': 'enter a decimal value.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_decimal_values': [None],
    'syntax': 'DECIMAL',
    'decimal_scale': None,
    'minimum_decimal': None,
    'maximum_decimal': None,
    'decimal_set': [],
}

PROFICIENCY_OBJECTIVE = {
    'element_label': 'objective',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

PROFICIENCY_RESOURCE = {
    'element_label': 'resource',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

PROFICIENCY_LEVEL = {
    'element_label': 'level',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}


