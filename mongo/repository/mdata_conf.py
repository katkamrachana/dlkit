"""Mongo osid metadata configurations for repository service."""

from .. import types
from ..primitives import Type
DEFAULT_LANGUAGE_TYPE = Type(**types.Language().get_type_data("DEFAULT"))
DEFAULT_SCRIPT_TYPE = Type(**types.Script().get_type_data("DEFAULT"))
DEFAULT_FORMAT_TYPE = Type(**types.Format().get_type_data("DEFAULT"))
DEFAULT_GENUS_TYPE = Type(**types.Genus().get_type_data("DEFAULT"))



ASSET_COPYRIGHT_REGISTRATION = {
    'element_label': 'copyright registration',
    'instructions': 'enter no more than 256 characters.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_string_values': [''],
    'syntax': 'STRING',
    'minimum_string_length': 0,
    'maximum_string_length': 256,
    'string_set': [],
}

ASSET_COPYRIGHT = {
    'element_label': 'copyright',
    'instructions': 'enter no more than 256 characters.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_string_values': [{
        'text': '',
        'languageTypeId': str(DEFAULT_LANGUAGE_TYPE),
        'scriptTypeId': str(DEFAULT_SCRIPT_TYPE),
        'formatTypeId': str(DEFAULT_FORMAT_TYPE),
        }],
    'syntax': 'STRING',
    'minimum_string_length': 0,
    'maximum_string_length': 256,
    'string_set': [],
}

ASSET_TITLE = {
    'element_label': 'title',
    'instructions': 'enter no more than 256 characters.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_string_values': [{
        'text': '',
        'languageTypeId': str(DEFAULT_LANGUAGE_TYPE),
        'scriptTypeId': str(DEFAULT_SCRIPT_TYPE),
        'formatTypeId': str(DEFAULT_FORMAT_TYPE),
        }],
    'syntax': 'STRING',
    'minimum_string_length': 0,
    'maximum_string_length': 256,
    'string_set': [],
}

ASSET_DISTRIBUTE_VERBATIM = {
    'element_label': 'distribute verbatim',
    'instructions': 'enter either true or false.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'syntax': 'BOOLEAN',
}

ASSET_CREATED_DATE = {
    'element_label': 'created date',
    'instructions': 'enter a valid datetime object.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_date_time_values': [None],
    'syntax': 'DATETIME',
    'date_time_set': [],
}

ASSET_DISTRIBUTE_ALTERATIONS = {
    'element_label': 'distribute alterations',
    'instructions': 'enter either true or false.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'syntax': 'BOOLEAN',
}

ASSET_PRINCIPAL_CREDIT_STRING = {
    'element_label': 'principal credit string',
    'instructions': 'enter no more than 256 characters.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_string_values': [{
        'text': '',
        'languageTypeId': str(DEFAULT_LANGUAGE_TYPE),
        'scriptTypeId': str(DEFAULT_SCRIPT_TYPE),
        'formatTypeId': str(DEFAULT_FORMAT_TYPE),
        }],
    'syntax': 'STRING',
    'minimum_string_length': 0,
    'maximum_string_length': 256,
    'string_set': [],
}

ASSET_PUBLISHED_DATE = {
    'element_label': 'published date',
    'instructions': 'enter a valid datetime object.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_date_time_values': [None],
    'syntax': 'DATETIME',
    'date_time_set': [],
}

ASSET_SOURCE = {
    'element_label': 'source',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

ASSET_PROVIDER_LINKS = {
    'element_label': 'provider links',
    'instructions': 'accepts an osid.id.Id[] object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': True,
    'default_id_values': [],
    'syntax': 'ID',
    'id_set': [],
}

ASSET_PUBLIC_DOMAIN = {
    'element_label': 'public domain',
    'instructions': 'enter either true or false.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'syntax': 'BOOLEAN',
}

ASSET_DISTRIBUTE_COMPOSITIONS = {
    'element_label': 'distribute compositions',
    'instructions': 'enter either true or false.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'syntax': 'BOOLEAN',
}

ASSET_COMPOSITION = {
    'element_label': 'composition',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}

ASSET_PUBLISHED = {
    'element_label': 'published',
    'instructions': 'enter either true or false.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'syntax': 'BOOLEAN',
}




ASSET_CONTENT_URL = {
    'element_label': 'url',
    'instructions': 'enter no more than 256 characters.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_string_values': [''],
    'syntax': 'STRING',
    'minimum_string_length': 0,
    'maximum_string_length': 256,
    'string_set': [],
}

ASSET_CONTENT_DATA = {
    'element_label': 'data',
    'instructions': 'accepts a valid data input stream.',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_object_values': [''],
    'syntax': 'OBJECT',
    'object_types': [],
    'object_set': [],
}

ASSET_CONTENT_ACCESSIBILITY_TYPE = {
    'element_label': 'accessibility type',
    'instructions': 'accepts an osid.type.Type object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_type_values': ['NoneType%3ANONE%40dlkit.mit.edu'],
    'syntax': 'TYPE',
    'type_set': [],
}

ASSET_CONTENT_ASSET = {
    'element_label': 'asset',
    'instructions': 'accepts an osid.id.Id object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': False,
    'default_id_values': [''],
    'syntax': 'ID',
    'id_set': [],
}




COMPOSITION_CHILDREN = {
    'element_label': 'children',
    'instructions': 'accepts an osid.id.Id[] object',
    'required': False,
    'read_only': False,
    'linked': False,
    'array': True,
    'default_id_values': [],
    'syntax': 'ID',
    'id_set': [],
}


